# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from itertools import groupby
import datetime

class AccountMove(models.Model):
    _inherit = 'account.move'

    def _default_branch_id(self):
        sale = self.env['sale.order'].search([('name', '=', self.invoice_origin)])
        inv = self.env['account.move'].search([('name', '=', self.ref)])
        if sale:
            branch_id = sale.branch_id.id
        elif self.purchase_id:
            branch_id = self.purchase_id.branch_id.id
        elif self.stock_move_id:
            branch_id = self.stock_move_id.branch_id.id
        elif inv:
            branch_id = inv.branch_id.id
        else:
            branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    # def _default_branch_id(self):
    #     sale = self.env['sale.order'].search([('name', '=', self.invoice_origin)])
    #     inv = self.env['account.move'].search([('name', '=', self.ref)])
    #     if sale:
    #         branch_id = sale.branch_id.id
    #     elif self.purchase_id:
    #         branch_id = self.purchase_id.branch_id.id
    #     elif self.stock_move_id:
    #         branch_id = self.stock_move_id.branch_id.id
    #     elif inv:
    #         branch_id = inv.branch_id.id
    #     else:
    #         branch_id = self.env['res.users'].browse(self._uid).branch_id.id
    #     return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id, string="Operating Unit", domain=lambda self: self._branch_domain())
    preselect = fields.Boolean("Preselect")

    def _branch_domain(self):
        """Ensure computed M2O domain work as expected."""
        ids = []
        for val in self.env.user.branch_ids:
            ids.append(val.id)
        domain = [('id', 'in', ids)]
        return domain

    @api.model
    def create(self, vals_list):
        res = super(AccountMove, self).create(vals_list)
        code = res.branch_id.invoice_sequence_id.code
        if res.type == 'out_invoice':
            seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(str(res.invoice_date)+" 00:00:00"))
            res.name = self.env['ir.sequence'].next_by_code(code, sequence_date=seq_date)
        return res

    # def unlink(self):
    #     branch_id = self.branch_id.id
    #     query = "select id from account_move where type='' and branch_id ="+str(branch_id)
    #     exe = self._cr.execute(query)
    #     sdfdsfsd
    #     res = super(AccountMove, self).unlink()
    #     return res

    # def action_post(self):
    #     res = super(AccountMove, self).create()
    #     code = self.branch_id.invoice_sequence_id.code
    #     if self.type == 'out_invoice' or self.type == 'in_invoice':
    #         self.name = self.env['ir.sequence'].next_by_code(code)
    #     return res

    # def action_post(self):
    #     res = super(AccountMove, self).create()
    #     code = self.branch_id.invoice_sequence_id.code
    #     if self.type == 'out_invoice' or self.type == 'in_invoice':
    #         self.name = self.env['ir.sequence'].next_by_code(code)
    #     return res

# partner_id = fields.Many2one('res.partner', readonly=True, tracking=True, states={'draft': [('readonly', False)]}, domain="[('customer_rank', '>', 0), '|', ('company_id', '=', False), ('company_id', '=', company_id)]", string='Customer', change_default=True)

# @api.onchange('line_ids')
# def _onchange_line_ids(self):
# 	for move in self:
# 		for line in move.line_ids:
# 			line.branch_id = move.branch_id.id

# def _recompute_dynamic_lines(self, recompute_all_taxes=False):
# 	res = super(AccountMove, self)._recompute_dynamic_lines(recompute_all_taxes=recompute_all_taxes)
# 	# self._onchange_branch_id()
# 	return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        if self.branch_id:
            customer = self.env['res.partner'].search([])
            users = self.env['res.users'].search([('branch_ids', 'in', self.branch_id.id)])
            if self.branch_id.id not in self.partner_id.branch_ids.ids:
                self.partner_id = False
                self.contact_partner_id = False
                self.contact_email = False
                self.title = False
                self.function = False
                self.contact_mobile = False
            if self.branch_id.id not in self.user_id.branch_ids.ids:
                self.user_id = False
            self.warehouse_id = self.env['stock.warehouse'].search([('branch_id', '=', self.branch_id.id)], limit=1)
            ids = []
            for value in customer:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'user_id': [('id', 'in', users.ids)],
                               'partner_id': [('id', 'in', ids)]}}

    # @api.onchange('partner_id')
    # def _onchange_partner_id(self):
    #     ids = []
    #
    #     if self.type == 'out_invoice':
    #         partner = self.env['res.partner'].search(['customer_rank','>', 0])
    #         if partner:
    #             for val in partner:
    #                 ids.append(val)
    #
    #     if self.type == 'in_invoice':
    #         partner = self.env['res.partner'].search(['supplier_rank','>',0])
    #         if partner:
    #             for val in partner:
    #                 ids.append(val.id)
    #
    #     return {'domain': {'partner_id': [('id', 'in', ids)]}}


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # @api.model
    # def create(self, vals_list):
    #     res = super(AccountMoveLine, self).create(vals_list)
    #     res.branch_id = self.move_id.branch_id.id
    #     return res

    def get_default_branch(self):
        # for i in self:
        #     sale = self.env['sale.order'].search([('name', '=', i.move_id.invoice_origin)])
        #     if sale:
        #         branch_id = sale.branch_id.id
        #         return branch_id
        #     if i.move_id.branch_id:
        return self.move_id.branch_id.id
        # else:
        #     return i.env.user.branch_id.id

    # branch_id = fields.Many2one('res.branch', string="Operating Unit", domain=lambda self: self._branch_domain(), default=lambda self:self.env.user.branch_id.id)
    branch_id = fields.Many2one('res.branch', string="Operating Unit", domain=lambda self: self._branch_domain(), default=lambda self: self.get_default_branch())
    invoice_no = fields.Char(string="Invoice Number")
    invoice_date = fields.Date(string="Invoice Date")
    partner_code = fields.Char(string="Partner Code")
    project_id = fields.Many2one('project.master', string="Project Code")
    bank_id = fields.Many2one('bank.master', string="Bank Code")

    def _branch_domain(self):
        """Ensure computed M2O domain work as expected."""
        ids = []
        for val in self.env.user.branch_ids:
            ids.append(val.id)
        domain = [('id', 'in', ids)]
        return domain

    # @api.onchange('partner_id')
    # def _onchange_partner_id(self):
    #     if self.partner_id:
    #         self.partner_code = self.partner_id.partner_code or False

    @api.onchange('account_id')
    def _onchange_account_id(self):
        if self.account_id:
            self.branch_id = self.move_id.branch_id.id


class ProjectMaster(models.Model):
    _name = 'project.master'
    _rec_name = 'code'

    code = fields.Char(string="Project Code")
    name = fields.Char(string="Project")
