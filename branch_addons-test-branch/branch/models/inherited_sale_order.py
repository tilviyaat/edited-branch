# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError

class SaleAdvancePaymentInv(models.TransientModel):

    _inherit = 'sale.advance.payment.inv'

    def create_invoices(self):
        res = super(SaleAdvancePaymentInv, self).create_invoices()
        move = self.env['account.move'].browse(res['res_id'])
        move_line = self.env['account.move.line'].search([('move_id', '=', res['res_id'])])
        for i in move_line:
            i.branch_id = move.branch_id.id
        # fgsgfsdgfsdf
        return res


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _get_default_warehouse_id(self):
        company = self.env.company.id
        user = self.env['res.users'].sudo().browse(self._uid)
        warehouse = self.env['stock.warehouse'].sudo().search([('branch_id', '=', user.branch_id.id)], limit=1)
        return warehouse.id

    warehouse_id = fields.Many2one(
        'stock.warehouse', string='Warehouse',
        required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        default=_get_default_warehouse_id, check_company=False)

    @api.model
    def create(self, vals_list):
        res = super(SaleOrder, self).create(vals_list)
        code = res.branch_id.quotation_sequence_id.code
        seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(res.date_order))
        res.name = self.env['ir.sequence'].next_by_code(code, sequence_date=seq_date)
        return res

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    @api.model
    def default_get(self, fields):
        res = super(SaleOrder, self).default_get(fields)
        user = self.env['res.users'].sudo().browse(self._uid)
        warehouse = self.env['stock.warehouse'].sudo().search([('branch_id', '=', user.branch_id.id)], limit=1)
        res['warehouse_id'] = False
        return res

    branch_id = fields.Many2one('res.branch', string="Operating Unit", default=_default_branch_id, required=True)

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.branch_id:
            customer = self.env['res.partner'].search([])
            ids = []
            for value in customer:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'partner_id': [('id', 'in', ids)]}}
        else:
            raise UserError("You have to select the branch.")

    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        res['branch_id'] = self.branch_id.id
        res['preselect'] = True
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        if self.branch_id:
            customer = self.env['res.partner'].search([])
            users = self.env['res.users'].search([('branch_ids', 'in', self.branch_id.id)])
            if self.branch_id.id not in self.partner_id.branch_ids.ids:
                self.partner_id = False
                self.contact_partner_id = False
                self.contact_email = False
                self.title = False
                self.function = False
                self.contact_mobile = False
            if self.branch_id.id not in self.user_id.branch_ids.ids:
                self.user_id = False
            self.warehouse_id = self.env['stock.warehouse'].search([('branch_id', '=', self.branch_id.id)], limit=1)
            ids = []
            for value in customer:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'user_id': [('id', 'in', users.ids)],
                               'branch_id': [('id', 'in', self.env.user.branch_ids.ids)],
                               'warehouse_id': [('branch_id', '=', self.branch_id.id)],
                               'partner_id': [('id', 'in', ids)]}}

class SaleOrderline(models.Model):
    _inherit = 'sale.order.line'

    def _prepare_invoice_line(self):
        self.ensure_one()
        res = super(SaleOrderline, self)._prepare_invoice_line()
        res['branch_id'] = self.order_id.branch_id.id
        return res

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.order_id.branch_id:
            ids = []
            vals = []
            template = self.env['product.template'].search([])
            for v in template:
                for branch in v.branch_ids:
                    if self.order_id.branch_id.id == branch.id:
                        ids.append(v.id)
            product = self.env['product.product'].search([('product_tmpl_id', 'in', ids), ('sale_ok', '=', True)])
            for val in product:
                vals.append(val.id)
            return {'domain': {'product_id': [('id', 'in', vals)]}}
        else:
            raise UserError("You have to select the branch.")
