from odoo import models, fields, api
from odoo.exceptions import UserError


class CRMBranch(models.Model):
    _inherit = 'crm.lead'

    def _default_branch_id(self):
        """Function to set current user branch id as default"""
        branch_id = self.env.user.branch_id.id
        return branch_id

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        res = super(CRMBranch, self)._onchange_partner_id()
        if self.branch_id:
            customer = self.env['res.partner'].search([('is_billable', '=', True)])
            ids = []
            for value in customer:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'partner_id': [('id', 'in', ids)]}}
        else:
            raise UserError("You have to select the branch.")
        return res

    branch_id = fields.Many2one('res.branch', string="Operating Unit", default=_default_branch_id, required=True)

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        if self.branch_id:
            customer = self.env['res.partner'].search([('is_billable', '=', True)])
            users = self.env['res.users'].search([('branch_id', '=', self.branch_id.id)])
            self.partner_id = False
            ids = []
            for value in customer:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'branch_id': [('id', 'in', self.env.user.branch_ids.ids)], 'partner_id': [('id', 'in', ids)]}}

# class SaleOrderline(models.Model):
#     _inherit = 'crm.lead.line'
#
#     @api.onchange('product_id')
#     def _onchange_product_id(self):
#         if self.crm_id.branch_id:
#             ids = []
#             vals = []
#             template = self.env['product.template'].search([])
#             for v in template:
#                 for branch in v.branch_ids:
#                     if self.crm_id.branch_id.id == branch.id:
#                         ids.append(v.id)
#             product = self.env['product.product'].search([('product_tmpl_id', 'in', ids), ('sale_ok', '=', True)])
#             for val in product:
#                 vals.append(val.id)
#             return {'domain': {'product_id': [('id', 'in', vals)]}}
#         else:
#             raise UserError("You have to select the branch.")
