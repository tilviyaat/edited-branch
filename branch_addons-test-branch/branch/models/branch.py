from odoo import fields, models, api, _

class ResBranch(models.Model):
    _name = 'res.branch'
    _description = 'Operation Unit'

    name = fields.Char(required=True)
    company_id = fields.Many2one('res.company', required=True)
    telephone = fields.Char(string='Telephone No')
    address = fields.Text('Address')
    email = fields.Text('Email')
    partner_id = fields.Many2one('res.partner', string="Related Partner")
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    quotation_sequence_id = fields.Many2one('ir.sequence', string="Quotation Sequence")
    invoice_sequence_id = fields.Many2one('ir.sequence', string="Invoice Sequence")
    picking_sequence_id = fields.Many2one('ir.sequence', string="Delivery Sequence")

    @api.model
    def create(self, vals_list):
        res = super(ResBranch, self).create(vals_list)
        seq = self.env['ir.sequence']
        quote_vals = {
            'name': _('%s QS') % (res.name),
            'code': _('res.branch.%s.quotation') % (res.name),
            'prefix': "Q/%(range_year)s/",
            'padding': 3,
            'company_id': res.company_id.id}
        quote = seq.create(quote_vals)
        inv_vals = {
            'name': _('%s Inv') % (res.name),
            'code': _('res.branch.%s.invoice') % (res.name),
            'prefix': "INV/%(range_year)s/",
            'padding': 3,
            'company_id': res.company_id.id}
        inv = seq.create(inv_vals)
        pick_vals = {
            'name': _('%s Pick') % (res.name),
            'code': _('res.branch.%s.delivery') % (res.name),
            'prefix': "D",
            'padding': 5,
            'company_id': res.company_id.id}
        pick = seq.create(pick_vals)
        res.quotation_sequence_id = quote.id
        res.invoice_sequence_id = inv.id
        res.picking_sequence_id = pick.id
        return res

    # def write(self, vals):
    #     seq = self.env['ir.sequence']
    #     if 'name' in vals:
    #         self.quotation_sequence_id.name = _('%s QS') % (vals['name'])
    #         self.quotation_sequence_id.code = _('res.branch.%s.quotation') % (vals['name'])
    #         self.invoice_sequence_id.name = _('%s inv') % (vals['name'])
    #         self.invoice_sequence_id.code = _('res.branch.%s.invoice') % (vals['name'])
    #     if not self.quotation_sequence_id:
    #         quote_vals = {
    #             'name': _('%s QS') % (vals['name']),
    #             'code': _('res.branch.%s.quotation') % (vals['name']),
    #             'prefix': "Q/%(range_year)s/",
    #             'padding': 3,
    #             'company_id': self.company_id.id}
    #         quote = seq.create(quote_vals)
    #         vals['quotation_sequence_id'] = quote.id
    #     if not self.invoice_sequence_id:
    #         inv_vals = {
    #             'name': _('%s inv') % (vals['name']),
    #             'code': _('res.branch.%s.invoice') % (vals['name']),
    #             'prefix': "INV/%(range_year)s/",
    #             'padding': 3,
    #             'company_id': self.company_id.id}
    #         inv = seq.create(inv_vals)
    #         vals['invoice_sequence_id'] = inv.id
    #     res = super(ResBranch, self).write(vals)
    #     return res


class Employee(models.Model):
    _inherit = 'hr.employee'

    branch_id = fields.Many2one('res.branch', string="Operating Unit", required=True)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    branch_ids = fields.Many2many('res.branch', 'product_template_branch_rel', 'product_temp_id', 'branch_id',
                                  string='Operating Units', required=True)

class EmployeePublic(models.Model):
    _inherit = 'hr.employee.public'

    branch_id = fields.Many2one('res.branch', string="Operating Unit", required=True)
