# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResUsers(models.Model):
    _inherit = 'res.users'

    branch_ids = fields.Many2many('res.branch', string="Allowed Operating Unit")
    branch_id = fields.Many2one('res.branch', string='Operating Unit')
