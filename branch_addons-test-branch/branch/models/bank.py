from odoo import models, fields


class BankMaster(models.Model):
    _name = 'bank.master'
    _rec_name = 'bank_code'

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id)
    name = fields.Char(string="Bank")
    bank_code = fields.Char(string="Bank Code")
    account_no = fields.Char(string="Account No.")

    _sql_constraints = [
        ('bank_code_uniq', 'unique (bank_code)', 'The bank code must be unique!')
    ]
