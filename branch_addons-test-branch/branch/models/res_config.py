from odoo import models, fields


class ResConfigSettingsInherit(models.TransientModel):
    _inherit = 'res.config.settings'

    branch_wise_sequence_sale = fields.Boolean('Operating Unit Wise Sequence')
