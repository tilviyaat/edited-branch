from odoo import api, fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id)

    # @api.onchange('type')
    # def onchange_type(self):
    #     if self.type not in ['cash', 'bank']:
    #         self.branch_id = False
