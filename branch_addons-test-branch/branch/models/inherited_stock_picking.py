# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api

READONLY_STATES = {
    'draft': [('readonly', False)],
}


class StockPicking(models.Model):

    _inherit = 'stock.picking'

    def _default_branch_id(self):
        if self.picking_type_id:
            branch_id = self.picking_type_id.branch_id.id
        else:
            branch_id = self.env['res.users'].browse(self._uid).branch_id.id

        return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id, readonly=True, states=READONLY_STATES)

    @api.onchange('partner_id', 'picking_type_id')
    def _related_location_branch(self):
        if self.picking_type_id.branch_id:
            branch = self.picking_type_id.branch_id.id
            return {
                'domain': {'location_id': [('branch_id', '=', branch)]}}
        else:
            ids = []
            for i in self.user_id.branch_ids:
                ids.append(i.id)
            branch = self.branch_id.id
            return {
                'domain': {'location_id': [('branch_id', '=', branch)], 'picking_type_id': [('branch_id', 'in', ids)]}}

    @api.onchange('branch_id')
    def _branch_location(self):
        if self.branch_id:
            branch = self.branch_id.id
            location = self.env['stock.location'].search([('branch_id', '=', branch)])
            if len(location) == 1:
                self.location_id = location.id
            else:
                self.location_id = False
            if self.picking_type_code:
                picking_type = self.env['stock.picking.type'].search(
                    [('code', '=', self.picking_type_code), ('branch_id', '=', self.branch_id.id)])
                self.picking_type_id = picking_type.id

            return {
                'domain': {'location_id': [('branch_id', '=', branch)],
                           'picking_type_id': [('branch_id', '=', branch)]}}


class StockPicking(models.Model):
    _inherit = 'stock.picking.type'

    branch_id = fields.Many2one('res.branch', related="warehouse_id.branch_id")


class StockLandedCost(models.Model):
    _inherit = "stock.landed.cost"

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id, required=True)


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    branch_id = fields.Many2one('res.branch', default=_default_branch_id)
