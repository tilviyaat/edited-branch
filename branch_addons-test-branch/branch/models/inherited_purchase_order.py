# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError


class purchase_order(models.Model):
    _inherit = 'purchase.order.line'

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    branch_id = fields.Many2one('res.branch', related='order_id.branch_id', default=_default_branch_id)

    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            for val in line._prepare_stock_moves(picking):
                val.update({
                    'branch_id': line.branch_id.id,
                })

                done += moves.create(val)
        return done

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.order_id.branch_id:
            ids = []
            vals = []
            template = self.env['product.template'].search([])
            for v in template:
                for branch in v.branch_ids:
                    if self.order_id.branch_id.id == branch.id:
                        ids.append(v.id)
            product = self.env['product.product'].search([('product_tmpl_id', 'in', ids)])
            for val in product:
                vals.append(val.id)
            return {'domain': {'product_id': [('id', 'in', vals)]}}
        else:
            raise UserError("You have to select the branch.")


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def _default_branch_id(self):
        branch_id = self.env['res.users'].browse(self._uid).branch_id.id
        return branch_id

    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrder, self).default_get(fields)
        user_branch = self.env['res.users'].browse(self.env.uid).branch_id
        if user_branch:
            branched_warehouse = self.env['stock.warehouse'].search([('branch_id', '=', user_branch.id)])
            if branched_warehouse:
                res['picking_type_id'] = branched_warehouse[0].in_type_id.id
            else:
                res['picking_type_id'] = False
        else:
            res['picking_type_id'] = False
        return res

    READONLY_STATES = {
        'draft': [('readonly', False)]
    }

    # partner_id = fields.Many2one('res.partner', string='supplier', required=True, states=READONLY_STATES, change_default=True, tracking=True)
    branch_id = fields.Many2one('res.branch', default=_default_branch_id, required=True, readonly=True,
                                states=READONLY_STATES)

    @api.onchange('branch_id')
    def onchange_branch_id(self):
        picking = self.env['stock.picking.type'].search(
            [('branch_id', '=', self.branch_id.id), ('code', '=', 'incoming')], limit=1)
        self.picking_type_id = picking.id if picking else False
        if self.branch_id:
            if self.branch_id.id not in self.partner_id.branch_ids.ids:
                self.partner_id = False
                self.partner_ref = False
                self.location = False
                self.contact_id = False
            vendor = self.env['res.partner'].search([('customer_rank', '=', 0)])
            ids = []
            for value in vendor:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'picking_type_id': [('branch_id', '=', self.branch_id.id), ('code', '=', 'incoming')],'partner_id': [('id', 'in', ids)]}}

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.branch_id:
            vendor = self.env['res.partner'].search([('customer_rank', '=', 0)])
            ids = []
            for value in vendor:
                if value.branch_ids:
                    for val in value.branch_ids:
                        if val.id == self.branch_id.id:
                            ids.append(value.id)
            return {'domain': {'partner_id': [('id', 'in', ids)]}}
        else:
            raise UserError("You have to select the branch.")

    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrder, self)._prepare_picking()
        res['branch_id'] = self.branch_id.id
        return res

    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        result['context']['default_branch_id'] = self.branch_id.id
        return result
