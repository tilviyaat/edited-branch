from odoo import models, fields


class ResPartner(models.Model):
	_inherit = 'res.partner'

	def _default_branch_id(self):
		branch_id = self.env['res.users'].browse(self._uid).branch_id.id
		if branch_id:
			return [(4, branch_id)]
		else:
			return False

	def _get_branch_domain(self):
		# if self.user_has_groups('hr_holidays.group_hr_holidays_user') or self.user_has_groups('hr_holidays.group_hr_holidays_manager'):
		#     return []
		# if self.user_has_groups('hr_holidays.group_hr_holidays_responsible'):
		#     return [('leave_manager_id', '=', self.env.user.id)]
		return [('id', 'in', self.env.user.branch_ids.ids)]


	branch_ids = fields.Many2many('res.branch', 'res_partner_branch_rel', 'partner_id', 'branch_id',
								  string='Operating Unit',
								  required=True, default=_default_branch_id, domain=_get_branch_domain)

class PartnerBank(models.Model):

	_inherit = "res.partner.bank"

	branch_id = fields.Many2one('res.branch', 'Branch')
