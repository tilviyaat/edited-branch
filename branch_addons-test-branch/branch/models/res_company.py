from odoo import models, fields


class ResCompanyInherit(models.Model):
    _inherit = "res.company"

    branch_wise_sequence_sale = fields.Boolean('Operating Unit Wise Sequence')
